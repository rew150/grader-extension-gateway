package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/wongtawan-j/grader-extension-module/kafka"
	"gitlab.com/wongtawan-j/grader-extension-module/message"
	"net/http"
	"strconv"
	"time"
)

type SubmissionRequest struct {
	StudentID  int `json:"studentID" binding:"required"`
	PublicKey  string `json:"publicKey" binding:"required"`
	SignedCode string `json:"signedCode" binding:"required"`
}

func (s *SubmissionRequest) toMessage() *message.Submission {
	return &message.Submission{
		EpochNano:  time.Now().UnixNano(),
		StudentID:  strconv.Itoa(s.StudentID),
		PublicKey:  s.PublicKey,
		SignedCode: s.SignedCode,
	}
}

func submissionHandler(c *gin.Context) {
	var submissionRequest SubmissionRequest
	if err := c.ShouldBindJSON(&submissionRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	kafka.ProduceSubmissionMessage(submissionRequest.toMessage().Serialize())
	c.Status(http.StatusOK)
}

type AssociationRequest struct {
	StudentID int `json:"studentID" binding:"required"`
	PublicKey string `json:"publicKey" binding:"required"`
}

func (s *AssociationRequest) toMessage() *message.Association {
	return &message.Association{
		EpochNano: time.Now().UnixNano(),
		StudentID: strconv.Itoa(s.StudentID),
		PublicKey: s.PublicKey,
	}
}

func associationHandler(c *gin.Context) {
	var associationRequest AssociationRequest
	if err := c.ShouldBindJSON(&associationRequest); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	kafka.ProduceAssociationMessage(associationRequest.toMessage().Serialize())
	c.Status(http.StatusOK)
}
