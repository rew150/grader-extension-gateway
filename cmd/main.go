package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
)

func main() {
	router := gin.Default()

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true

	router.Use(cors.New(config))

	router.POST("/submit", submissionHandler)
	router.POST("/create", associationHandler)
	router.GET("/healthcheck", func (c *gin.Context) {
		c.Status(http.StatusOK)
	})

	router.Run(":" + os.Getenv("HTTP_PORT"))
}
