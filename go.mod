module grader-extension-gateway

go 1.15

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	gitlab.com/wongtawan-j/grader-extension-module v1.0.14
)
