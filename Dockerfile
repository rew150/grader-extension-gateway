FROM golang:1.15

WORKDIR /app/gateway

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o ./out/app ./cmd

CMD ["./out/app"]